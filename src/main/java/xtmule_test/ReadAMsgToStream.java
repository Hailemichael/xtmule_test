package xtmule_test;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import org.apache.commons.lang3.ArrayUtils;

import xware.xtapi.IMsg;
import xware.xtapi.XTException;

public class ReadAMsgToStream {
	public ByteArrayInputStream streamIMsg (IMsg msg) throws XTException{
		ByteArrayInputStream bis = null;
		String msgInfo = null;
		ArrayList<byte[]> msgWithInfo = new ArrayList<>();
		if(msg != null){
				
				msgInfo = "\n==============================================="
					+ "\nMessageID: " + String.valueOf(msg.getHandle()) 
					+ "\nMessageInfo: " + msg.getInfo() 
					+ "\nMessageProperties: " + msg.getProperties() + "\n";
				System.out.print(msgInfo);
				//Concat these two info
				msgWithInfo.add(msgInfo.getBytes());
				msgWithInfo.add(msg.getData());
			
			
			byte[] byteArray = ArrayUtils.toPrimitive(msgWithInfo.toArray(new Byte[msgWithInfo.size()]));
			
			bis = new ByteArrayInputStream(byteArray);
		}						
		
		return bis;
	}
}
