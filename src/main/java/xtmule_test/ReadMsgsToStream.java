package xtmule_test;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import org.apache.commons.lang3.ArrayUtils;

import xware.xtapi.IMsg;
import xware.xtapi.XTException;

public class ReadMsgsToStream {	
	
	public ByteArrayInputStream streamIMsg (IMsg[] msgs) throws XTException{
		ByteArrayInputStream bis = null;
		String msgInfo = null;
		ArrayList<byte[]> msgWithInfo = new ArrayList<>();
		if(msgs != null){
			for(int i=0; i < msgs.length; i++){
				
				msgInfo = "\n==============================================="
					+ "\nMessageID: " + String.valueOf(msgs[i].getHandle()) 
					+ "\nMessageInfo: " + msgs[i].getInfo() 
					+ "\nMessageProperties: " + msgs[i].getProperties() + "\n";
				System.out.print(msgInfo);
				//Concat these two info
				msgWithInfo.add(msgInfo.getBytes());
				msgWithInfo.add(msgs[i].getData());
			}
			
			byte[] byteArray = ArrayUtils.toPrimitive(msgWithInfo.toArray(new Byte[msgWithInfo.size()]));
			
			bis = new ByteArrayInputStream(byteArray);
		}						
		
		return bis;
	}
}
